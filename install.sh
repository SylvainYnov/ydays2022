# Script automatisation d'installation 
#!/bin/sh 

# Update des dépôts & Upgrade des paquets 
sudo apt update && sudo apt upgrade -y 

# Installation des paquets nécessaires 
apt -y install apache2 docker docker-compose fail2ban 

# Copie des fichiers de configuration pour Apache 
cp conf_files/vhost.conf /etc/apache2/sites-available/000-default.conf 
cp conf_files/ports.conf /etc/apache2/ports.conf 

# Copie du fichier de jails pour le fail2ban 
cp conf_files/jails.fail2ban.conf /etc/fail2ban/jail.d/custom.conf 

# Restart du service fail2ban & Apache 
sudo service apache2 restart && sudo service fail2ban restart 

# Création des dossiers du docker NPM 
sudo mkdir nginx-proxy-manager 

# Copie du fichier docker-compose dans le dossier NPM 
cp conf_files/docker-compose.yml nginx-proxy-manager/docker-compose.yml

# Déploiement du docker-compose 
docker-compose -f nginx-proxy-manager/docker-compose.yml up -d

# Installation de Portainer 
docker run -d -p 8000:8000 -p 9000:9000 --restart=unless-stopped --name="Portainer" -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer-ce

# Création du conteneur de mise à jour dynamique 
sudo docker container run -d -e USERNAME=acafssl -e PASSWORD=QfPi7#3qQY?C -e "DOMAINS=acafssl.ddns.net" -e INTERVAL=5 aanousakis/no-ip
