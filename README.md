### Clonage du repository
```git clone https://gitlab.com/SylvainYnov/ydays2022.git```

### Se déplacer dans le dossier du projet
```cd ydays2022/```

### Ajouter les droits d’exécution sur le script d’installation
```chmod +x install.sh```

### Exécuter le script d’installation avec les droits sudo
```Sudo ./install.sh```

